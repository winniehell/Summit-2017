# Summit FAQ

1. What happens if I need to travel through the US to get to Mexico?

If you need a US Transit Visa (ESTA) and have an e-passport you can find more information [here](https://www.cbp.gov/travel/international-visitors/frequently-asked-questions-about-visa-waiver-program-vwp-and-electronic-system-travel). 

1. If my passport is near expiration, will GitLab pay for my renewal?

GitLab will assist with an visa related expenses, but not passport expenses. For more information, please ask People Ops. 

1. Please add to me.