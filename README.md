**Destination**: Cancun (CUN), Mexico!
<br>
**Confirmed** dates: Fly in on January 10th - depart on 17th 2017
<br>
We're staying at the Barcelo Hotel

Please make sure to let us know **BEFORE OCTOBER 1ST** through >> [**this form**](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdu3dFjMhqt-VoTejd2CqneWAQCxob38rMyVpgKqoqdUhjN0Q/viewform) << if:
1. you will or cannot stay the full week
1. you plan to bring your SO (and/or kids) or not
1. if there is any specific reason you cannot share a room (apart from bringing your SO)

<br>
Please understand that **everyone will share a room** with a team member with the exeption of bringing an SO or specific reason.
If you choose **not** to want to share you will have to pay the difference to get a private room; **~$250 personally (not expensable)**
<br>
<br>
<br>
<br>
Make sure to prepare yourself well and check out:

- Please check a box in our [SO poll](http://poll.fm/5ni1r) so we can make sure to have enough rooms :)

- [CDC Health information](http://wwwnc.cdc.gov/travel/destinations/traveler/none/mexico)
- [CDC info on the Zika virus](http://wwwnc.cdc.gov/travel/notices/alert/zika-virus-mexico)
    - Specific info for women who are (trying to get) pregnant: [here](http://www.cdc.gov/zika/pregnancy/thinking-about-pregnancy.html)
- [Visa info]( https://mexico.visahq.com/)
- Overview for getting visa, flights and briging your SO is in [this sheet](https://docs.google.com/spreadsheets/d/12MqT7aBHHGPes45Q9jTNyYRTTx9or7FPNTEl3HWJwoE/edit#gid=1198457175)